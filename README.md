# Nodejs project starter

* Eslint (Using [Airbnb](https://github.com/airbnb/javascript))
* Babel (with [stage-3](https://www.npmjs.com/package/babel-preset-stage-3) preset)
* Nodemon 

### Npm commands

* `npm run start` to watch main entry file(index.js) and execute with babel.
* `npm run build` build/transpile source files with babel into the `dist` folder.

### Project structure
```
├── src
|   ├── Javascript files
├── dist
|   ├── (final distribution files)
|
├── (Directory root)

```